const mongoose = require("mongoose");
var moment = require("moment");
const { Schema } = mongoose;

const ParteAveriaSchema = new Schema({
  
  num_aviso: { type: String, uppercase: true, required: true },
  fecha_aviso: { type: Date},
  cif_cliente: {type: Number},
  zona: { type: Boolean, required: true, },
  decripcion: { type: String, uppercase: true},
  estado: {type: String},
  observaciones_tecnico: { type: String, uppercase: true},
  intervencion_1:[
        {
        fecha:{ type: Date },
        num_albaran: { type: Number},
        status_averia:{ type: String},
        }
    ]
  
});
