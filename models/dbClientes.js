const mongoose = require("mongoose");
var moment = require("moment");
const { Schema } = mongoose;

const ClientesSchema = new Schema({
  cif: { type: String, uppercase: true, required: true },
  num_cliente: {type: Number},
  nombre: { type: String, uppercase: true, required: true, },
  direccion: { type: String },
  cod_postal: { type: Number },
  poblacion: { type: String },
  provincia: { type: String },
  email: { type: String },
  telefono: { type: String },
  //
  sitios: [
    {
      num_registro_industrial: { type: String },
      nom_comercial: { type: String },
      direccion: { type: String },
      cod_postal: { type: Number },
      localidad: { type: String }
    }
  ]
});

/* ******metodos para instacias de ClientesSchema ****  */
/* ClientesSchema.methods.list_clientes = async function (params) {
  
}; */

const MaquinasSchema = new Schema({
  cif: { type: String },
  num_registro_industrial: { type: String },
  categoria: { type: String },
  marca: { type: String, uppercase:true },
  modelo: { type: String, uppercase:true },
  num_serie: { type: String },
  cod_aprob_modelo: { type: String },
  ano_fabricacion: { type: String },
  //
  anexo3: [
    {
      num_anexo: { type: Number },
      fecha: {
        type: Date,
        required: true,
        /* cambio el formato de fecha a DD/MM/YYYY cuando se envia el Json hacia el font-end(Cliente)  */
        get: function(format_fecha) {
          // console.log('fecha:',format_fecha);
          var dateFormat = format_fecha.toISOString().substring(0, 10);
          return moment(dateFormat).format("DD/MM/YYYY");
        }
      },
      precintos: [
        {
          localizacion: { type: String },
          num_precinto: {
            type: String,
            set: function(prefijo) {
              /* console.log(prefijo); */
              if (!prefijo) {
                return prefijo;
              } else {
                if (
                  prefijo.indexOf("04-H-") !== 0 &&
                  prefijo.indexOf("04-H-") !== 0
                ) {
                  prefijo = "04-H-000" + prefijo;
                }
                return prefijo;
              }
            },
            get: function(prefijo) {
              if (!prefijo) {
                return prefijo;
              } else {
                if (
                  prefijo.indexOf("04-H-") !== 0 &&
                  prefijo.indexOf("04-H-") !== 0
                ) {
                  prefijo = "04-H-000" + prefijo;
                }
                return prefijo;
              }
            }
          }
        }
      ]
    }
  ]
});

/* Establecer campo virtual para fecha  */
// MaquinasSchema.virtual('fecha_format')

//   .get(function(){
//     return this.anexo3.fecha.toISOString().substring(0,10);
//   });

MaquinasSchema.set("toJSON", { getters: true, virtuals: true });

module.exports.clientes = mongoose.model("Clientes", ClientesSchema);
module.exports.maquinas = mongoose.model("Maquinas", MaquinasSchema);
