const mongoose = require('mongoose');
const { Schema } = mongoose;
const moment = require('moment');

 

const UserSchema = new Schema({
    cif:{type: String, required:true},
    fecha: {
        type: Date, 
        required: true,
          /* cambio el formato de fecha a DD/MM/YYYY cuando se envia el Json hacia el font-end(Cliente)  */
          get: function(formato_fecha) {
            // console.log('fecha:',format_fecha);
            var dateFormat = formato_fecha.toISOString().substring(0, 10);
            return moment(dateFormat).format("DD/MM/YYYY");
          }
    },
    num_aviso:{type: String, required: false},
    descripcion:{type: String, uppercase: true, required: false},
    cliente: {type: String, uppercase: true, required: true},
    h_ini: {type: String, required: true },
    km_ini: {type:Number, required: false },
    h_fin: {type: String, required: true},
    km_fin: {type:Number, required: true },
    localidad:{type: String, uppercase: true, required: false},
    tecnico:{type:  String},
    vehiculo:{type: String, uppercase: true},
    tipo_ruta:{type: String},
    usuario:{type: String}
});


module.exports = mongoose.model('HojaRuta', UserSchema);
