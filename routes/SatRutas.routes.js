var express = require("express");
var router = express.Router();
const Rutas = require("../models/dbHojaRuta");
const Clientes = require("../models/dbClientes").clientes;
const passport = require("passport");
const { isAuthenticated } = require("../helpers/Athoriz");
const axios = require("axios");

/*** ADD_RUTAS **** 
* Abre la pagina Añadir rutas y envio la consulta de todos los clientes 
* para realizar la busqueda en el SELECT CLIENTE 
*/ 
router.get("/add_ruta", isAuthenticated, async function(req, res, next) {
  const client = await Clientes.find({}, { nombre: 1, cif: 1 , poblacion: 1});
  res.render("SAT_Somaitec/Rutas/addRutas", {client});
});


/* GET listado Diario de Rutas, la consulta la realizo sobre el usuario  *  registrado en la sesión
*/
router.get("/ListRutas", isAuthenticated, async function(req, res, next) {
  const rutas = await Rutas.find({usuario: req.user.id}).sort({ fecha: "desc", h_ini:"desc" });
  res.render("SAT_Somaitec/Rutas/DiarioRuta", { rutas });
});
/* ************************** */

/* ***** Editar Rutas********* */
router.put("/editRuta/:id", isAuthenticated, async (req, res) => {
  const _id = req.params.id;

  console.log(_id);
});
/* **************************** */

// ELIMINAR RUTA PARA VUE
router.delete("/delete_Ruta/:id", async (req, res) => {
  const _id = req.params.id;
  try {
    const rutaDb = await Rutas.findByIdAndDelete({ _id });
    if (!rutaDb) {
      return res.status(400).json({
        mensaje: "No se encontro el Ruta ",
        error
      });
    }
    res.redirect("/rutas/ListRutas");
    // res.json(rutaDb);
  } catch (error) {
    return res.status(400).json({
      mensaje: "hay un error",
      error
    });
  }
});
/* **************************************** */

/* *** POST ADD RUTAS*** 
*  Guardo los datos de la ruta del formulario AÑADIR RUTA con el usuario 
*  que se ha registrado en la sesión actual
*/
router.post("/add_rutas", isAuthenticated, async (req, res) => {
  const {
    fecha,
    cif,
    h_ini,
    km_ini,
    num_aviso,
    cliente,
    localidad,
    descripcion,
    h_fin,
    km_fin
  } = req.body;
  console.log(req.body);

  const newRuta = new Rutas({
    fecha,
    cif,
    h_ini,
    km_ini,
    num_aviso,
    cliente,
    localidad,
    descripcion,
    h_fin,
    km_fin
    
  });
  newRuta.usuario = req.user.id; /* En el campo usuario --> el usuario
                                  * de la sesión actual */
  await newRuta.save();
  req.flash("success_msg", "Has añadido una nueva ruta");
  res.redirect("/rutas/ListRutas");
});
// MODIFICAR Ruta
router.get("/modif_ruta/:id", isAuthenticated, async(req, res, next) =>{
  const ruta = await Rutas.findById(req.params.id);
  console.log(ruta);
  res.render('SAT_Somaitec/Rutas/modRuta',{ruta});
});
router.put('/modif_ruta/:id', isAuthenticated, async(req, res, next) =>{
  const {
    fecha,
    h_ini,
    km_ini,
    num_aviso,
    cliente,
    localidad,
    descripcion,
    h_fin,
    km_fin
  } = req.body;
  await Rutas.findByIdAndUpdate(req.params.id,{  /*Busco por id y *modificos los valores
  *siguientes : 
  */
    fecha,
    h_ini,
    km_ini,
    num_aviso,
    cliente,
    localidad,
    descripcion,
    h_fin,
    km_fin
  });
  res.redirect("/rutas/ListRutas");
});
// ELIMINAR Ruta
router.get("/del_ruta/:id", isAuthenticated, async (req, res, next) => {
  const _id = req.params.id;
  try {
    const rutaDb = await Rutas.findByIdAndDelete({_id });
    return res.redirect("/rutas/ListRutas");

  } catch (error) {
    console.log(error);
    return res.status(400).json({
      mensaje: "Ocurrio un error",
      error
    });
  }
});

// **********RUTAS PARA VUE *************************

// GET listado de rutas para VUE
router.get("/vue_listRutas", async (req, res) => {
  try {
    const vue_rutas = await Rutas.find().sort({ fecha: "desc", h_ini:"desc" });
    res.json(vue_rutas);
  } catch (error) {
    return res.status(400).json({
      mensaje: "Ocurrio unn error",
      error
    });
  }
});
// GET una sola ruta para  VUE
router.get("/vueGetRuta/:id", async (req, res) => {
  const _id = req.params.id;
  console.log(_id);
  try {
    const rutaDB = await Rutas.findOne({ _id });
    console.log(rutaDB);
    //res.render("SAT_Somaitec/DiarioRuta", { rutaDB });
    res.json(rutaDB);
  } catch (error) {
    return res.status(400).json({
      mensaje: "No se pudeo recuperar la ruta",
      error
    });
  }
});
// POST Añadir ruta para VUE --------
router.post("/add_vueRuta", async (req, res, next) => {
  const body = req.body;
  console.log(body);
  try {
    const rutaDB = await Rutas.create(body);
    res.status(200).json(rutaDB);
  } catch (error) {
    return res.status(500).json({
      mensaje: "hay un error",
      error
    });
  }
});

//DELETE Eliminar ruta para VUE -------------
router.delete("/del_vueRuta/:id", async (req, res) => {
  const _id = req.params.id;
  try {
    const rutaDb = await Rutas.findByIdAndDelete({ _id });
    if (!rutaDb) {
      return res.status(400).json({
        mensaje: "No se encontro el Ruta ",
        error
      });
    }
    res.json(rutaDb);
  } catch (error) {
    return res.status(400).json({
      mensaje: "hay un error",
      error
    });
  }
});

// PUT Modificar ruta VUE ---------
router.put("/put_vueRuta/:id", async (req, res) => {
  const _id = req.params.id;
  const body = req.body;
  console.log(body);

  try {
    const rutaDB = await Rutas.findByIdAndUpdate(_id, body, { new: true });
    res.json(rutaDB);
  } catch (error) {
    return res.status(400).json({
      mensaje: "hay un error",
      error
    });
  }
});



module.exports = router;
