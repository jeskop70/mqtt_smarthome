var express = require("express");
var router = express.Router();
const Clientes = require("../models/dbClientes").clientes;
const Maquinas = require("../models/dbClientes").maquinas;
const passport = require("passport");
const { isAuthenticated } = require("../helpers/Athoriz");
const axios = require("axios");
/*find Clientes*/

/* **************************** */
/* RUTA PARA BUSCAR POR CLIENTE */
/* **************************** */
router.get("/get_cliente", isAuthenticated, async function(req, res, next) {
  const client = await Clientes.find({}, { _id: 0, cif: 1 });
  console.log("entra: ", client);
  res.send(client);
});

/* ************************ */
/* RUTA PARA BUSCAR MAQUINA */
/* ************************ */
router.get("/get_maquina/:cif", isAuthenticated, async function(  req, res, next) {
  const cif_b = req.params.cif;
  console.log("buscar maquina cif: ", cif_b);
  const maquina = await Maquinas.find({ cif: cif_b });
  //console.log(maquina);
  return res.status(200).json({ maquina });
});
/* ********************************************* */
/** Reg maquina / surtidor ... guardar Surtidor  */
/* ********************************************* */
router.post("/reg_surtidor", isAuthenticated, async (req, res) => {
  console.log(req.body);
  const newSurtidor = new Maquinas();
  newSurtidor.cif = req.body.cif;
  newSurtidor.categoria = req.body.categoria;
  newSurtidor.marca = req.body.marca;
  newSurtidor.modelo = req.body.modelo;
  newSurtidor.num_serie = req.body.num_serie;
  newSurtidor.ano_fabricacion = req.body.ano_fabricacion;
  newSurtidor.cod_aprob_modelo = req.body.cod_aprob_modelo;

  //guardar surtidor
  try {
    await newSurtidor.save();
    req.flash("success_msg", "Has añadido un nuevo Surtidor");
    
    return res.status(200).json({
      mensaje:"Has añadido un Surtidor"
    });
  } catch (error) {
    console.log(error);
  }
});

/* **************************************************** */
/* * CONSULTA a DB si existe el mismo nº de ANEXO3 **** */
/* **************************************************** */
router.post("/existe_anexo3/", isAuthenticated,  async function(req, res){
  const anexo = req.body.num_anexo;
  console.log("servidor: ",req.body);
  const dbRes = await Maquinas.findOne({'anexo3.num_anexo' : anexo });
  console.log("depues de consulta", dbRes);
  if (dbRes) {
      return res.status(200).json({
      existe: 1
    });
  } else {
    return res.status(200).json({
      existe: 0
    });
  }
  
});
/* ****************************************************** */
/** CONSULTA a DB si existe el mismo nº de precinto ***** */
/* ****************************************************** */
router.post("/existe_precinto/", isAuthenticated,  async function(req, res){
  const precinto = req.body.num_precinto;
  console.log("servidor: ",req.body);
  //console.log("buscar precinto : ", precinto);
  const dbRes = await Maquinas.findOne({'anexo3.precintos.num_precinto' : precinto });
  console.log("depues de consulta", dbRes);
  if (dbRes) {
      return res.status(200).json({
      existe: 1
    });
  } else {
    return res.status(200).json({
      existe: 0
    });
  }
  
});
/* ******************************************** */
/* reg maquina / precintos .. guardar precintos */
/* ******************************************** */
router.post("/reg_precintos", isAuthenticated, async (req, res) => {
  console.log(req.body);
  var doc;
  
  const new_anex = [
    {
      fecha: req.body.fecha,
      num_anexo: req.body.num_anexo,
      precintos: req.body.precintos
    }
  ];
    console.log(new_anex);

   let registro = await Maquinas.findOneAndUpdate({cif: req.body.cif, num_serie: req.body.num_serie}, { $push: { anexo3 : new_anex} }, {
    new: true
  });

  return res.status(200).json({
    mensaje:"ok"
  });
});

/* ADD PRECINTO */
/* En esta ruta se llama a la vista de añadir precintos y se envia la consulta con todos los clientes */
router.get("/add_precintos", isAuthenticated, async function(req, res, next) {
 //var urlback = req.originalUrl;
 const client = await Clientes.find({}, { nombre: 1, cif: 1 });
  res.render("SAT_Somaitec/Precintos/addPrecintos", { client });
});

/*** GET listado DE PRECINTOS *******/
router.get("/list_precintos", isAuthenticated, async function(req, res, next) {
  console.log("entraprecintos");

   const precintos = await Maquinas.find({'anexo3.precintos.num_precinto':{ $exists:true }} , {'cif':1 ,'num_serie':1,'fecha_format':1, 'anexo3.num_anexo':1 ,'anexo3.fecha':1,'anexo3.precintos.num_precinto':1, 'anexo3.precintos.localizacion': 1 },{ 'sort': { 'anexo3.num_anexo': 'asc', 'anexo3.precintos.num_precinto':'asc'}});
  //console.log(precintos[0].anexo3[0]);
  //console.log(precintos);
  res.render("SAT_Somaitec/Precintos/listaPrecintos", { precintos });
});
/* ------------------- */

/* MODIFICAR PRECINTOS */
router.put("/editPrecinto/:id", isAuthenticated, async (req, res) => {
  const _id = req.params.id;
  console.log(_id);
});
/* -------------------- */

// ELIMINAR RUTA PARA VUE
router.delete("/delete_precinto/:id", async (req, res) => {
  const _id = req.params.id;
  try {
    const rutaDb = await Precintos.findByIdAndDelete({ _id });
    if (!rutaDb) {
      return res.status(400).json({
        mensaje: "No se encontro el Precinto ",
        error
      });
    }
    res.redirect("/precintos/ListPrecintos");
    
  } catch (error) {
    return res.status(400).json({
      mensaje: "hay un error",
      error
    });
  }
});

// POST ADD PRECINTO
router.post("/add_precintos", isAuthenticated, async (req, res) => {
  const { fecha, num_anexo3, cliente, localizacion } = req.body;
  console.log(req.body);

  const newPrecintvo = new Maquinas({
    num_anexo: num_anexo3,
    fecha: fecha,
    localizacion
    //tecnico
  });
  newPrecinto.precintos = array;
  await newPrecinto.save();
  req.flash("success_msg", "Has añadido un nuevo Precinto");
  res.redirect("/precintos/add_precintos");
});

// ELIMINAR precinto
router.get("/del_precinto/:id", isAuthenticated, async (req, res, next) => {
  const _id = req.params.id;
  try {
    const rutaDb = await Precintos.findByIdAndDelete({ _id });
    return res.redirect("/precinto/ListPrecintos");
  } catch (error) {
    console.log(error);
    return res.status(400).json({
      mensaje: "Ocurrio un error",
      error
    });
  }
});


module.exports = router;
