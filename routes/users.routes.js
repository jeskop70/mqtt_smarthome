
var express = require('express');
var router = express.Router();
const User = require('../models/dbUser');
const passport = require('passport');
const {isAuthenticated} = require('../helpers/Athoriz');
const {isAuthenticatedAdmin} = require('../helpers/Athoriz');

/* GET users listing. */
router.get('/registro', isAuthenticatedAdmin, function(req, res, next) {
  res.render('user/registro');
});
//GET listado de Usuarios
router.get('/listUser', isAuthenticated, async function(req, res, next) {
  const user = await User.find();
  res.render('user/lis_user', {user});
});
// GET login
router.get('/log', function(req, res, next) {
  res.render('user/login');
 });
// EDITAR Usuario
router.get('/edit/:id', async (req, res) => {
  const user = await User.findById(req.params.id)
  res.render('user/editUser', {user});
});
router.put('/edit-user/:id', async(req, res) =>{
  const {role, usuario, email, password} = req.body;
  await User.findByIdAndUpdate(req.params.id, {role, usuario, email, password});
  res.redirect('/users/listUser')
});
  
// ELIMINAR usuario
router.delete('/delete/:id', isAuthenticated, async (req, res, next) => {
  await User.findByIdAndDelete(req.params.id);
  res.redirect('/users/listUser/');
});
// AUTETIFICAR Usuario
router.post('/log', passport.authenticate('local', {
  successRedirect: '/',
  failureRedirect: '/users/log/',
  failureFlash: true,
}));
// REGISTRO de Usuarios
router.post('/registro',  async(req, res) => {
  const {role, usuario, email, password} = req.body;
  const errors = [];
  console.log(req.body);
  
  if(usuario.length <= 0) {
    errors.push({text: 'Por favor inserta un nombre de usuario'});
  } 
  if(password.length < 4) {
    errors.push({text: 'La contraseña debe tener al menos 4 carácteres'});
  }
  if(errors.length > 0) {
    res.render('user/registro', {errors, usuario, email, password});
  } else {
  const emailUser = await User.findOne({email: email});
  if (emailUser) {
    req.flash('error_msg', 'El email esta en uso');
    res.redirect('/users/registro');
  }
  const newUser = new User({role ,usuario, email, password});
  newUser.password = await newUser.encryptPassword(password);
  await newUser.save();
  req.flash('success_msg', 'Te has registrado correctamente');
  res.redirect('/');
  
  }
});
//Finalizar la Sesión
router.get('/logout', (req, res) => {
  req.logout();
  req.flash('success_msg', 'Has finalizado la sesión');
  res.redirect('/users/log');
})

module.exports = router;

  