var express = require('express');
var router = express.Router();

const {isAuthenticated} = require('../helpers/Athoriz');

router.get('/quieneres', isAuthenticated, function(req, res, next) {
  res.send(req.user);
  //res.render('index', { title: 'Pagina de Inicio' , user: req.user});
});  

/* GET home page. */
router.get('/', isAuthenticated, function(req, res, next) {
  
  res.render('index', { title: 'Pagina de Inicio' , user: req.user});
});  

/*GET LOGIN*/
router.get('/login', function(req, res, next) {
  res.render('login');
});

module.exports = router;
