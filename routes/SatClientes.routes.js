var express = require("express");
var router = express.Router();
const Clientes = require("../models/dbClientes").clientes;
const Maquinas = require("../models/dbClientes").maquinas;
const passport = require("passport");
const { isAuthenticated } = require("../helpers/Athoriz");
const axios = require("axios");

/* direccion de inicio de ruta /clientRouter */

/*RUTA PARA BUSCAR POR CLIENTE */
router.get("/buscar_cliente", isAuthenticated, async function(req, res, next) {
  const client = await Clientes.find({}, { _id: 0, cif: 1, nombre: 1, num_cliente:1 });
  console.log("entra: ", client);
  // res.send(client);
  res.render('SAT_Somaitec/Clientes/buscarCliente', {client});
});
/* Consulta de Cliente segun el cif */
router.get('/get_cliente/:cif', isAuthenticated, async function(req,res){
  const cif = req.params.cif;
  console.log(cif);
  try {
    const client_cif = await Clientes.find({cif: cif});
    res.json(client_cif);
  } catch (error) {
    return res.status(400).json({
      mensaje: "Ocurrio un error al leer el cliente",
      error
    });
  }

});
/* ************************************ */

/*****LISTAR CLIENTES********/
router.get('/list_clientes', isAuthenticated, async function(req, res, next) {
    const client = await Clientes.find().sort({ nombre: "desc"});
    //console.log("entra: ", client);
    res.render('SAT_Somaitec/Clientes/listClientes', {client});
  });

/* IR ADD_CLIENTE 
   ruta para abrir la pagina fomulario añadir cliente
*/  
router.get("/addCliente", isAuthenticated, function(req, res, next) {
  console.log(req.originalUrl);
  console.log(req.baseUrl);
  res.render("SAT_Somaitec/Clientes/addCliente");
});
// ********POST CLIENTE************

/*AÑADIR NUEVO CLIENTE *
* Guarda nuevo cliente en la base de datos
*/
router.post("/add_cliente", isAuthenticated, async (req, res) => {
  const {
    num_cliente,
    cif,
    nombre,
    direccion,
    cod_postal,
    poblacion,
    provincia,
    email,
    telefono
  } = req.body;
  
  const newClientes = new Clientes({
    num_cliente,
    cif,
    nombre,
    direccion,
    cod_postal,
    poblacion,
    provincia,
    email,
    telefono
  });

  newClientes.save(function(err, doc) {
    if (!err) {
      console.log("ok", doc);
      req.flash("success_msg", "Has añadido un nuevo Cliente");
    } else {
      console.log(err);
    }
  });
  res.redirect("/");
});


  /* ****Editar Cliente************ */
router.get('/edit_cliente/:id', isAuthenticated, async(req, res) =>{
  const cliente = await Clientes.findById(req.params.id);
  console.log(cliente);
  res.render('SAT_Somaitec/Clientes/editCliente', {cliente});
});
  /* ********************************** */
 
  /* ****Modificar Cliente ****/
router.put('/edit_cliente/:id', async(req, res) =>{
  const {num_cliente, cif, nombre, direccion, cod_postal, poblacion, provincia, email, telefono} = req.body;
  console.log(req.body);
  await Clientes.findByIdAndUpdate(req.params.id, {num_cliente, cif, nombre, direccion, cod_postal, 
  poblacion, provincia, email, telefono});  
  res.redirect('/clientes/list_clientes')
  });  
/* **************************** */

/* Exixte num_cliente */
router.post("/existe_cliente/:num_clie", isAuthenticated, async function(req, res){
  const numCliente = req.params.num_clie;
  console.log("servidor: ",req.body);
  //console.log("buscar precinto : ", precinto);
  const dbRes = await Clientes.findOne({'num_cliente':numCliente });
  console.log("depues de consulta", dbRes);
  if (dbRes) {
      return res.status(200).json({
      existe: 1
    });
  } else {
    return res.status(200).json({
      existe: 0
    });
  }
  
});
/* ****************** */
/* MAQUINAS */
router.get('/get_maquinas/:cif', isAuthenticated, async function(req,res){
  const cif = req.params.cif;
  console.log(cif);
  try {
    const maquinas_cif = await Maquinas.find({cif: cif});
    res.json(maquinas_cif);
  } catch (error) {
    return res.status(400).json({
      mensaje: "Ocurrio un error al leer el cliente",
      error
    });
  }

});
/* ************** */

/* Consulta Maqina por nº de serie */
router.get('/get_maquina/:id', isAuthenticated, async function(req,res){
  const id = req.params.num_serie;
  console.log(id);
  try {
    const maquinaID = await Maquinas.find({_id: id });
    res.json(maquinaID);
  } catch (error) {
    return res.status(400).json({
      mensaje: "Ocurrio un error al leer la Maquina",
      error
    });
  }

});
/* ******************************* */
router.put("/update_maquina/:id", isAuthenticated, async function(req, res) {
  const id = req.params.id;
  const {categoria, marca, num_serie, modelo, ano_fabricacion, cod_aprob_modelo} = req.body;
  console.log(id);

  try {
    const maquinaDB = await Maquinas.findOneAndUpdate({_id: id}, {categoria, marca, num_serie, modelo, ano_fabricacion, cod_aprob_modelo}, { new: true });
    // conosole.log({_id: id});
    res.status(200).json(maquinaDB);
 
  } catch (error) {
      return res.status(200).json({
        mensaje: "hay un error",
        error
    });
  }
});


/* Exixte cif */

/* ****************** */

module.exports = router;
