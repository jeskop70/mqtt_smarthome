const helpers = {};

helpers.isAuthenticated = (req, res, next) => {
    if(req.isAuthenticated()){
        return next();
    }
    // req.flash('error_msg', 'Debes Introducir tus Datos');
    res.redirect('/users/log/');
}

helpers.isAuthenticatedAdmin = (req, res, next) => {
    if(req.isAuthenticated()){
        if (req.user.role=="admin"){
            return next();
        }
    }
    // req.flash('error_msg', 'Debes Introducir tus Datos');
    res.redirect('/users/log/');
}

module.exports = helpers;