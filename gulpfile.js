
const gulp = require('gulp');
const sass = require('gulp-sass'); 
const browserSync = require('browser-sync').create();
const nodemon = require('gulp-nodemon');

/* Tareas */

/* Compila los archivos sass de bootstrap y los de la carpeta sass
*  y los pone en la carpeta de stylesheets ya compilados */

gulp.task('sass', () => {
    return gulp.src([
        'node_modules/bootstrap/scss/bootstrap.scss',
        // 'node_modules/select2-theme-bootstrap4/src/scss/.scss',
        './sass/*.scss' 
    ])
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('public/stylesheet'))
    .pipe(browserSync.stream());

});

gulp.task('sass:watch', function(done){
    gulp.watch("./sass/*.scss", gulp.series('sass'));
    
    done();
});

gulp.task('views:watch', function(done){
    gulp.watch("views/*.hbs").on('change',browserSync.reload);
    console.log('observando views');
    done();
});



/* copia los archivos js de boottap, jquery y popper a la carpeta public/javascripts */
gulp.task('js', () =>{
    return gulp.src([
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/@popperjs/core/dist/umd/popper.min.js',
        'node_modules/select2/dist/js/select2.min.js'
    ])
    .pipe(gulp.dest('public/javascripts'))

});

/* Ejecutar el arranque del servidor con Nodemon */
gulp.task('nodemon', (cb) =>{
    var started = false;

    return nodemon({
        script: "./bin/www",
        ignore: ["node_modules/*", "public/*, views/*"]
    }).on('start', () => {
        if(!started) {
            cb();
            started = true;
        }
    });
});

/* Servidor de desarrollo + seguir los cambios de archivos sass y html */
gulp.task('browser-sync',  (cb) => {

    browserSync.init(null, {
            proxy: "http://localhost:3000",
            files: ["public/**/*.*", "views/**/*.hbs"],
            // browser: "google crhome",
            port: 3001,
            reloadDelay: 1000, //Important, otherwise syncing will not work
            });
    cb();
     
});

/* Definir tarea por defecto, y dev(ejecuta servidor de desarrollo) */
gulp.task('dev', gulp.series('nodemon', 'browser-sync'))
gulp.task('default',
    gulp.series('nodemon','browser-sync',  gulp.parallel('js','sass:watch')));
