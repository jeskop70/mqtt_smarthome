/*Programa principal Cliente WEB  */
var delRuta = {};

/* Array categoria Maquinas */
var opcion_cat_maquinas = [
  {id:0, text: 'Surtidor'},
  {id:1, text: 'Dispensador'},
  {id:2, text: 'Camión Cisterna'},
  {id:3, text: 'Puente de Lavado'},
  {id:4, text: 'Centro de Lavado'},
  {id:5, text: 'Aspirador'},
  {id:6, text: 'Aux. Lavado'},
  {id:7, text: 'Compresor'},
  {id:8, text: 'Aire-Agua'},
  {id:9, text: 'Control Desatendidas'},
];
/*  Si el modal añadir maquinas es llamado, desde la pagina añadir precintos. El
 *valor del campo categoria, solo de ser de aparatos surtidores o camiones cisterna  */
var opcion_cat_surtidores = [
  {id:1, text: 'Surtidor'},
  {id:2, text: 'Dispensador'},
  {id:3, text: 'Camión Cisterna'}
];

// var prec = $("#num_precinto").val();
// var arrayprecintos = [];
// var selected; // Variable global para guardar el cif del cliente seleccionado form Añadir Precintos
/**funciones que se ejecutan cuando la pagina se termina de cargar */
$("document").ready(function() {
  //$("#modal_error").modal("show"); //Funcion para pressentar modales de error
  // Presetar modal para filtrar tabla de Rutas
  $("#modalBuscar").click(function() {
    $("#filtro-tabla").modal("show");
  });

  // /* Al Cambiar el valor de select cliente PAGE AÑADIR PRECINTOS */
  // $("#select_client").change(function() {
  //   selected = $("#select_client option:selected").val();
  //   console.log(selected);
  //   // limpiarSelect(select_client);
  //   //var det = (document.getElementById("select_surtidor").length = 0);
  //   //console.log(det.length);
  //   // $("#select_surtidor").empty(); //Elimino los datos del select_Surtidor anteriores
  //   valIniSelect("select_surtidor");
  //   querySurtidores(selected); /* Realizo la consulta de los          *surtidores que tiene el cliente y actualiza la lista del campo     *select_surtidor */
  //   $("#date").removeAttr("disabled"); //habilitar Form Datos AnexoIII
  //   $("#num_anexo-3").removeAttr("disabled"); //habilitar Form Datos AnexoIII
  // });

  // //}); /* fin document ready() */

  // /** AL PERDER EL FOCO DEL INPUT Nº DE ANEXOIII */
  // $("#num_anexo-3").focusout(function() {
  //   if ($("#num_anexo-3").val() > 0) {
  //     var data = {
  //       etiqueta: "anexo",
  //       mensaje: "El Nº de AnexoIII. Existe en la  Base de Datos"
  //     };

  //     var anexoActual = { num_anexo: $("#num_anexo-3").val() };
  //     axios
  //       .post("/precintos/existe_anexo3", anexoActual)
  //       .then(res => {
  //         console.log("datos -->", res.data.existe);
  //         if (res.data.existe == 1) {
  //           //$("#num_anexo-3").val("");
  //           $("#select_surtidor").attr("disabled", "disabled");
  //           $("#btn_add_surtidor").attr("disabled", "disabled");
  //           window.scrollTo(0, 0);
  //           mensajeError(data);
  //         } else {
  //           $("#select_surtidor").removeAttr(
  //             "disabled"
  //           );
  //         /* Habilitar formulario de identificación de Surtidor*/
  //           $("#btn_add_surtidor").removeAttr("disabled");
  //           // $("#select_surtidor").focus();
  //         }
  //       })
  //       .catch(err => {
  //         console.log(err);
  //       });
  //   } else {
  //     $("#select_surtidor").attr("disabled", "disabled");
  //     $("#btn_add_surtidor").attr("disabled", "disabled");
  //     $("#num_precinto").attr("disabled", "disabled");
  //     $("#localizacion").attr("disabled", "disabled");
  //     $("#add_precintos").attr("disabled", "disabled");

  //     document.getElementById("num_anexo-3").focus();
  //   }
  // });

  // /**Al cambiar el valor de Selec_surtidor */
  // $("#select_surtidor").change(function() {
  //   $("#num_precinto").removeAttr("disabled");
  //   $("#localizacion").removeAttr("disabled");
  //   $("#add_precintos").removeAttr("disabled");
  // });
  // // ----------------------------
}); /**--fin de document ready-------------- */

/**codigo para pagina de DIARIO DE RUTAS */
async function open_modal(id) {
  console.log(id);
  try {
    var template = $("#elimruta").html();
    const res = await axios.get(
      `/rutas/vueGetRuta/${id}`
    ); /* realiza la consulta del registro seleccionado */
    console.log(res.data);
    delRuta = res.data;
    var rutaEliminar =
      " " +
      delRuta.fecha +
      " / " +
      delRuta.num_aviso +
      " / " +
      delRuta.cliente +
      " / " +
      delRuta.localidad +
      " ...";
    document.getElementById("rut_eliminar").value = rutaEliminar;
    $("#confir-eliminar").modal("show"); //visualiza modal
  } catch (error) {
    console.log(error);
  }
}

$("#confir-eliminar").on("show.bs.modal", function() {
  console.log("Modal Show: ", delRuta._id);
});

// CAPTURA EVENTO CLICK DEL BOTON "ACEPTAR" DE MODAL CONFIRMAR ELIMINAR
$("#btAceptar").on("click", function(valor, e) {
  $("#confir-eliminar").modal("hide");
  console.log("pulsado eliminar: ", valor);
  elimnarRuta(delRuta.id);
});

// ELIMINAR RUTA SELECCIONADA
function elimnarRuta(id) {
  axios
    .get(`/rutas/del_ruta/${delRuta._id}`)
    .then(res => {
      window.location.href = "/rutas/ListRutas/";
    })
    .catch(err => {
      console.log(err);
    });
}
/**<---------------------------------> */

/*****Evento al cerrar modal_error ****/

$("#modal_error").on("hidden.bs.modal", function(e) {
  $("#num_precinto").val("");
  $("#num_precinto").focus();
});
// **********

/** CODIGO PARA GUARDAR PRECINTOS */

$("#btGuardar").on("click", function() {
  event.preventDefault();

  var fecha = $("#date").val();
  var numAnexo = $("#num_anexo-3").val();
  var cifCliente = $("#select_client").val();
  var serieSurt = $("#select_surtidor").val();
  var precintos = arrayprecintos;
  console.log(cifCliente);
  data = {
    fecha: fecha,
    num_anexo: numAnexo,
    cif: cifCliente,
    num_serie: serieSurt,
    precintos: precintos
  };

  axios
    .post("/precintos/reg_precintos", data)
    .then(res => {
      console.log(res.mensaje);
      document.write(res.mensaje);
    })
    .catch(err => {
      console.log(err);
    });
});

// /* Abrir formulario modal para añadir surtidor */
// $("#btn_add_surtidor").on("click", function() {
//   $("#modal_add_Surtidor").modal("show");
// });

// $("#modal_add_Surtidor").on("shown.bs.modal", function() {
//   var focus_marca = document.getElementById("marca");
//   $("#cif_client").val(selected);
//   focus_marca.focus();
// });
// /**GUARGAR DATOS DE NUEVO SURTIDOR EN BASE DE DATOS*/
// $("#btn_guardar_surtidor").on("click", function() {
//   event.preventDefault();

//   var cif = $("#cif_client").val();
//   var marca = $("#marca").val();
//   var num_serie = $("#num_serie").val();
//   var modelo = $("#modelo").val();
//   var año_fabric = $("#año_fabr").val();
//   var aprob_modelo = $("#aprob_modelo").val();
//   data = {
//     cif: cif,
//     marca: marca,
//     num_serie: num_serie,
//     modelo: modelo,
//     ano_fabicacion: año_fabric,
//     cod_aprob_modelo: aprob_modelo
//   };
//   axios
//     .post("/precintos/reg_surtidor", data)
//     .then(res => {
//       console.log(res);
//       console.log("hemos vuelto");
//       $("#cif_client").val("");
//       $("#marca").val("");
//       $("#num_serie").val("");
//       $("#modelo").val("");
//       $("#año_fabr").val("");
//       $("#aprob_modelo").val("");
//       $("#modal_add_Surtidor").modal("hide");
//     })
//     .catch(err => {
//       console.log(err);
//     });
// });
// /* CUANDO SE OCULTA EL MODAL DE AÑADIR SURTIDOR */
// $("#modal_add_Surtidor").on("hidden.bs.modal", function(e) {
//   console.log("se ha ocultado modal add precintos");
//   valIniSelect("select_surtidor"); /* Inicializo la lista de select_surtidor */
//   querySurtidores(
//     selected
//   ); /* Realizo la consulta de los surtidores  que tiene el
//       * cliente y actualiza la lista del campo select_surtidor */
// });
// /* *****fin******** */

/** FUNCIONES */

/* Funcion Inicializar el campo de selección de surtidores */
function valIniSelect(idSelect) {
  document.getElementById(
    idSelect
  ).length = 0; /* Limpio la lista de select_surtidor  */
  var select = document.getElementById(idSelect);
  var option = document.createElement("option");
  option.text = "Introduce Nº de surtidor";
  select.add(option, select[0]);
  console.log("select");
}
/* ---------fin function valIniSelect()--------------- */

/* Consulta de Surtidores de un cliente */
function querySurtidores(cif_cliente) {
  var client = cif_cliente;
  axios.get(`/precintos/get_maquina/${client}`).then(function(res) {
    console.log(res.data.maquina.length);
    let options = "";
    for (var i = 0; i < res.data.maquina.length; i++) {
      options +=
        '<option value="' +
        res.data.maquina[i].num_serie +
        '">' +
        res.data.maquina[i].num_serie +
        "</option>";
    }
    $("#select_surtidor").append(options); //visualiza la lista de Surtidores del Cliente
  });
}
/* -------------fin function querySurtidores()--------------- */

/**Visualizar modal de error */
function mensajeError(data) {
  var mens = data.mensaje;
  console.log(mens);
  $("#errmsg").text(mens);
  $("#modal_error").modal("show");
  switch (data.etiqueta) {
    case "anexo":
      $("#modal_error").on("hidden.bs.modal", function(e) {
        $("#num_anexo-3").focus();
      });
      break;
    case "precinto":
      $("#modal_error").on("hidden.bs.modal", function(e) {
        $("#num_precinto").focus();
      });
    case "valid_Cif":
      $("#modal_error").on("hidden.bs.modal", function(e) {
        $("#cif").focus();
      });

      break;
  }
}
/**---------fin function mensajeError()------------ */

/* *********** Validación del campo CIF/NIF *******/

/* A - Sociedades Anónimas
B - Sociedades de responsabilidad limitada
C - Sociedades colectivas
D - Sociedades comanditarias
E - Comunidades de bienes
F - Sociedades cooperativas
G - Asociaciones y otros tipos no definidos
H - Comunidades de propietarios
J - Sociedades civiles, con o sin personalidad jurídica
K - Españoles menores de 14 años
L - Españoles residentes en el extranjero sin DNI
M - NIF que otorga la Agencia Tributaria a extranjeros que no tienen NIE
N - Entidades extranjeras
P - Corporaciones locales
Q - Organismos autónomos
R - Congregaciones e instituciones religiosas
S - Organos de la administración
U - Uniones Temporales de Empresas
V - Otros tipos no definidos en el resto de claves
W - Establecimientos permanentes de entidades no residentes en España
X - Extranjeros identificados por la Policía con un número de identidad de extranjero, NIE, asignado hasta el 15 de julio de 2008
Y - Extranjeros identificados por la Policía con un NIE, asignado desde el 16 de julio de 2008 (Orden INT/2058/2008, BOE del 15 de julio )
Z - Letra reservada para cuando se agoten los 'Y' para Extranjeros identificados por la Policía con un NIE

La ultima cifra es el dígito de control, que puede ser o bien un número o bien
una letra, en función del tipo de sociedad.
A las categorias P (Ayuntamientos) y X (Extranjeros) les corresponde una letra
en lugar de un número.

El dígito de control se calcula con las 7 cifras restantes del CIF (quitando la
primera y la ultima), con el siguiente algoritmo:

- CIF: A58818501
- Quitamos la primera y la ultima cifra:
	5881850
- Sumamos las cifras pares:
	Suma = 8 + 1 + 5 = 14
- Ahora sumamos cada cifra impar multiplicada por dos, y sumamos las cifras del
  resultado:
	5 * 2 = 10 ==> 1 + 0 = 1
	8 * 2 = 16 ==> 1 + 6 = 7
	8 * 2 = 16 ==> 1 + 6 = 7
	0 * 2 = 0 ==> 0
- y volvemos a sumar esos resultados a la suma anterior:
	Suma=Suma+1+7+7+0;
- Al final de este proceso, tenemos que Suma=29, pues bien, nos quedamos con la
  cifra de las unidades (9)
- Restamos esta cifra de las unidades de 10, dándonos un 1, que es el código de
  control para todos los tipos de sociedades exceptuando la X que se verifica
  como un DNI.
- Para las sociedades K, P, Q y S habria que sumar un 64 al digito de control que
  hemos calculado para hallar el ASCII de la letra de control:
	Chr(64+(10-(Suma mod 10)))
 */

/*
 * Tiene que recibir el cif sin espacios ni guiones
 */
function validateCIF(cif) {
  //Quitamos el primer caracter y el ultimo digito
  var valueCif = cif.substr(1, cif.length - 2);

  var suma = 0;

  //Sumamos las cifras pares de la cadena
  for (i = 1; i < valueCif.length; i = i + 2) {
    suma = suma + parseInt(valueCif.substr(i, 1));
  }

  var suma2 = 0;

  //Sumamos las cifras impares de la cadena
  for (i = 0; i < valueCif.length; i = i + 2) {
    result = parseInt(valueCif.substr(i, 1)) * 2;
    if (String(result).length == 1) {
      // Un solo caracter
      suma2 = suma2 + parseInt(result);
    } else {
      // Dos caracteres. Los sumamos...
      suma2 =
        suma2 +
        parseInt(String(result).substr(0, 1)) +
        parseInt(String(result).substr(1, 1));
    }
  }

  // Sumamos las dos sumas que hemos realizado
  suma = suma + suma2;

  var unidad = String(suma).substr(1, 1);
  unidad = 10 - parseInt(unidad);

  var primerCaracter = cif.substr(0, 1).toUpperCase();

  if (primerCaracter.match(/^[FJKNPQRSUVW]$/)) {
    //Empieza por .... Comparamos la ultima letra
    if (
      String.fromCharCode(64 + unidad).toUpperCase() ==
      cif.substr(cif.length - 1, 1).toUpperCase()
    )
      return true;
  } else if (primerCaracter.match(/^[XYZ]$/)) {
    //Se valida como un dni
    var newcif;
    if (primerCaracter == "X") newcif = cif.substr(1);
    else if (primerCaracter == "Y") newcif = "1" + cif.substr(1);
    else if (primerCaracter == "Z") newcif = "2" + cif.substr(1);
    return validateDNI(newcif);
  } else if (primerCaracter.match(/^[ABCDEFGHLM]$/)) {
    //Se revisa que el ultimo valor coincida con el calculo
    if (unidad == 10) unidad = 0;
    if (cif.substr(cif.length - 1, 1) == String(unidad)) return true;
  } else {
    //Se valida como un dni
    return validateDNI(cif);
  }
  return false;
}

/*
 * Tiene que recibir el dni sin espacios ni guiones
 * Esta funcion es llamada
 */
function validateDNI(dni) {
  var lockup = "TRWAGMYFPDXBNJZSQVHLCKE";
  var valueDni = dni.substr(0, dni.length - 1);
  var letra = dni.substr(dni.length - 1, 1).toUpperCase();

  if (lockup.charAt(valueDni % 23) == letra) return true;
  return false;
}

/* document.write("<br>CIF: "+validateCIF("a58818501"));
document.write("<br>DNI: "+validateDNI("38119995w")); */
