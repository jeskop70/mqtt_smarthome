const mqtt = require('mqtt');
var estados = {
  piscina : {
    "SB_depuradora": null,
    "SB_llenaPiscina": null,
    "SB_focosPiscina": null,
    "SB_dosifPH": null
  }

};


var client  = mqtt.connect();
 
client.on('connect', function () {
  client.subscribe('piscina/luz', {qos: 0});
  client.subscribe('clientes', {qos: 0});
  
})
 
client.on('message', function (topic, message) {

  if (topic=="piscina/luz") {
    var mensaje = message.toString();
    
    if (IsJsonString(mensaje)){
      var jsmensaje = JSON.parse(mensaje);

      console.log("parseado: "+ jsmensaje);
  
      estados.piscina.SB_depuradora = jsmensaje.uno;
      estados.piscina.SB_llenaPiscina = jsmensaje.dos;
      estados.piscina.SB_focosPiscina = jsmensaje.uno;
      estados.piscina.SB_dosifPH = jsmensaje.uno;

      console.log(estados);
    }
   
  }

  if (topic=="clientes") {
    var mensaje = "";
    var user_web = "";
    var jsonUser = {};

    if (message=="clienteweb"){
      client.publish('piscina/estado', JSON.stringify(estados));
    } 
    mensaje = message.toString();
    console.log(mensaje);
    console.log("mensage1: " + message.toString());
    for(i=27; i<mensaje.length; i++){
      user_web += (mensaje[i]);
    }
    jsonUser = JSON.parse(user_web);
    console.log(user_web);
    console.log(jsonUser);
    console.log("topic: "+ topic);
    console.log(jsonUser.user);
  }

});

/*

class MqttHandler {
  constructor() {
    this.mqttClient = null;
    this.host = null;
   // this.username = 'YOUR_USER'; // mqtt credentials if these are needed to connect
   // this.password = 'YOUR_PASSWORD';
  }
  
  sendMessage(message) {
    this.mqttClient.publish('piscina/estado', message);
  }

  connect() {
    // Connect mqtt with credentials (in case of needed, otherwise we can omit 2nd param)
    this.mqttClient = mqtt.connect(this.host, { username: this.username, password: this.password });

    // Mqtt error calback
    this.mqttClient.on('error', (err) => {
      console.log(err);
      console.log("estoy aqui");
      this.mqttClient.end();
    });

    // Connection callback
    this.mqttClient.on('connect', () => {
      console.log(`mqtt client connected`);
      // console.log("connected flag  " + mqttClient.connected);
      // mqtt subscriptions
      this.mqttClient.subscribe('piscina/luz', {qos: 0});
      this.mqttClient.subscribe('clientes', {qos: 0});
      this.mqttClient.publish('piscina/estado', "estadoinicial");
   
    });

    // When a message arrives, console.log it
    this.mqttClient.on('message', function (topic, message) {
    
     // this.mqttClient.publish('piscina/estado', "estadoinicial");

    if (topic=="piscina/luz") {
      var mensaje = message.toString();
      
      if (IsJsonString(mensaje)){
        var jsmensaje = JSON.parse(mensaje);

        console.log("parseado: "+ jsmensaje);
        //estados.piscina={"uno": jsmensaje.uno , "dos": jsmensaje.dos };
    
        estados.piscina.SB_depuradora = jsmensaje.uno;
        estados.piscina.SB_llenaPiscina = jsmensaje.dos;
        estados.piscina.SB_focosPiscina = jsmensaje.uno;
        estados.piscina.SB_dosifPH = jsmensaje.uno;


        console.log(estados);
      }
     
    }

    //console.log(this);
    if (topic=="clientes") {
      if (message=="clienteweb"){
        this.mqttClient.publish('piscina/estado', "estadoinicial");
       
      } 
     
    }

      console.log("mensage1: " + message.toString());
      console.log("topic: "+ topic);
    });

    this.mqttClient.on('close', () => {
      console.log(`mqtt client disconnected`);
    });
  }

  // Sends a mqtt message to topic: mytopic
}

module.exports = MqttHandler;

*/

function IsJsonString(str) {
  try {
      JSON.parse(str);
  } catch (e) {
      return false;
  }
  return true;
}