$(function() {
  console.log("has cargado codigo js para Clientes");

  /* NUM_CLIENTE EXISTE?? */
  /* Al cambiar value del input num_cliente verifico que no existe
   * en la base de datos. Utilizo el evento change en la página de
   * Editar Cliente     */
  $("#nume_client").change(function() {
    
    if ($("#nume_client").val() > 0) {
      var data = {
        etiqueta: "numero_cliente",
        mensaje: "El Nº de cliente ya Existe en la Base de Datos"
      };

      var numClientActual = $("#nume_client").val(); 
      console.log('que ', numClientActual);
      axios
        .post(`/clientes/existe_cliente/${numClientActual}`)
        .then(res => {
          console.log("datos -->", res.data.existe);
          if (res.data.existe == 1) {
           
            mensajeError(data); // visualiza el mensaje de error
            $("#nume_client").val("");
            $("#nume_client").focus();
           
          } else {
            //$("#inp_client").focus();
          }
        })
        .catch(err => {
            alert(err);
        });
    } else {
        $('#nume_client').val('');
        alert('Debes introducir un Nº de cliente');
        document.getElementById("nume_client").focus();
    }
  });

  /* ******************** */

  /* CIF EXISTE ?? */
  /* Al perder el foco en el input cif validamos el cif  */
  $("#cif").focusout(function() {
    var data = {
        etiqueta: "valid_Cif",
        mensaje: "El Cif no válido"
      };
    cif_actual = $('#cif').val();
    var validCif = validateCIF(cif_actual);
    if (!validCif) {
       mensajeError(data); // visualiza el mensaje de error
    }
    else{
        validCif = validateCIF(cif_actual);
        if (!validCif) {
            mensajeError(data); // visualiza el mensaje de error
        }
    }
  });
  /* ************* */

}); // Fin de document ready


/* Funcion para volver a pgina anterior */
function fn_regresar() {
  window.history.back();
}
