/* CODIGO para la pagina BUSCAR CLIENTE/MAQUINAS */

var res_Cliente = {};
var clie_seleccionado='';
var maquina_seleccionada='';
var id_maquina_selec = '';
var res_Maquinas=[];
var categoria ='';

$(function(){
    var estado = 0;
    $("#sel_buscarCliente").select2 ({ 
        theme : "bootstrap"       
    });
    $('#sidebarCollapse').on('click', function(){
        $('#sidebar').toggleClass('active');
    }) 
    $('#men_principal').toggleClass('visualizar', estado);
    
    /* evento que se produce al seleccionar un elemento de select2 */
    $('#sel_buscarCliente').on('select2:select', function (e) {
        clie_seleccionado = e.params.data.id;
        console.log("ID seleccionado: " + e.params.data.id);
        if (clie_seleccionado=='') {
            console.log("vacio");
            $('#form_edit_cliente').addClass('ver');
            $('#list-maquinas').addClass('ver');    
        } else {
            $('#form_edit_cliente').removeClass('ver');
            // desabilFormCliente();
            $('#list-maquinas').removeClass('ver');
            queryCliente(clie_seleccionado);
        }
    });

    
    $('#select_categoria').on('select2:select', function (e) {
         categoria = e.params.data.text;
         console.log('select_categoria' + e.params.data.text);
    });
    


}); /* Fin document.ready */

/* METODOS */
/* Consulta de Cliente, segun el cif del cliente seleccionado */
function queryCliente(cif_cliente_select) {
    var cif = cif_cliente_select;
    axios
        .get(`/clientes/get_cliente/${cif}`)
        .then(function(res) {
            // console.log('de regreso ', res);
            res_Cliente = res.data;
            // console.log(res_Cliente);
            llenarCampos(res_Cliente);
            queryMaquinas(cif);
        })
        .catch(err => {
            console.log(err);
        });

}
/* funcion para rellenar los campos del formulario cliente */
function llenarCampos(cliente) {
    console.log(cliente);
    $('#nume_client').val(cliente[0].num_cliente);   
    $('#cif').val(cliente[0].cif);   
    $('#inp_client').val(cliente[0].nombre);   
    $('#direccion').val(cliente[0].direccion);   
    $('#cod_postal').val(cliente[0].cod_postal);   
    $('#localidad').val(cliente[0].poblacion);   
    $('#provincia').val(cliente[0].provincia);   
    $('#email').val(cliente[0].email);   
    $('#telefono1').val(cliente[0].telefono);   
}
/* función para consultar las maquinas del cliente seleccionado */
function queryMaquinas(cif_selec) {
    axios
        .get(`/clientes/get_maquinas/${cif_selec}`)
        .then(function(res) {
            //console.log('de regreso ', res);
            res_Maquinas = res.data;
            console.log(res_Maquinas);
            llenarTabla(res_Maquinas);
        
    })
    .catch(err => {
        console.log(err);
      }); 
};
/* LLenar Tabla maquinas */
function llenarTabla(_maquina) {
    $("#table_body").html("");
    for(var i=0; i<_maquina.length; i++){
    var tr = `<tr>
      <td>`+_maquina[i].categoria+`</td>
      <td>`+_maquina[i].marca+`</td>
      <td>`+_maquina[i].modelo+`</td>
      <td>`+_maquina[i].num_serie+`</td>
 
      <td><button id="btn_edit_maquina" onclick="abrir_modal('`+_maquina[i].id+`')" type="button" class="btn btn-warning btn-sm"><i class="far fa-edit">edit</i></a></td>
    </tr>`;
    $("#table_body").append(tr);
    /* Al hacer click en un boton de editar maquina de la
     *lista de maquinas */
   
  }
    
};
/* ******** FIN METODOS ********** */

/***********************
* - CRUD DE MAQUINAS - *
************************/

/* EDITAR MAQUINA  */
function abrir_modal(id) {           // Esta función esta asociada al boton edit de la tabla de maquina lin(96)
                                     // onclick="abrir_modal('`+_maquina[i].id+`') 
    //console.log(parsemaquina);
    maquina_seleccionada = id;
    console.log(maquina_seleccionada);
    
    $("#modal_edit_maquina").modal("show");
    
}

$("#modal_edit_maquina").on("shown.bs.modal", function() {
    var indi= -1; 

    for (i=0; i < res_Maquinas.length; i++){
        if( res_Maquinas[i]._id == maquina_seleccionada){
            indi=i;
            console.log(indi);
            

        }
    }
    id_maquina_selec = res_Maquinas[indi]._id;
    console.log(res_Maquinas[indi]._id);
    
    $('#edit_cif_client').val(res_Maquinas[indi].cif);
    $('#edit_categoria').val(res_Maquinas[indi].categoria);
    $('#edit_marca').val(res_Maquinas[indi].marca);
    $('#edit_num_serie').val(res_Maquinas[indi].num_serie);
    $('#edit_modelo').val(res_Maquinas[indi].modelo);
    $('#edit_año_fabr').val(res_Maquinas[indi].ano_fabricacion);
    $('#edit_aprob_modelo').val(res_Maquinas[indi].cod_aprob_modelo);
    
    $('#edit_categoria').focus();
});
  /* ********************* */
/* ABRIR FORMULARIO MODAL para AÑDIR SURTIDOR */
$("#btn_add_maquina").on("click", function() {
    $("#modal_add_Surtidor").modal("show");
  });
  
  $("#modal_add_Surtidor").on("shown.bs.modal", function() {
    
    $('#select_categoria').select2 ({
        theme : "bootstrap",
        placeholder: 'Selecciona una categoria',
        data: opcion_cat_maquinas
    });

    var focus_categoria = document.getElementById("select_categoria");
    $("#cif_client").val(clie_seleccionado);
    focus_categoria.focus();
  });

  /**GUARGAR DATOS DE NUEVO SURTIDOR EN BASE DE DATOS*/
  $("#btn_guardar_surtidor").on("click", function() {
    event.preventDefault();
   
  
    var cif = $("#cif_client").val();
    var marca = $("#marca").val();
    var num_serie = $("#num_serie").val();
    var modelo = $("#modelo").val();
    var año_fabric = $("#año_fabr").val();
    var aprob_modelo = $("#aprob_modelo").val();
    data = {
      cif: cif,
      categoria: categoria,
      marca: marca,
      num_serie: num_serie,
      modelo: modelo,
      ano_fabricacion: año_fabric,
      cod_aprob_modelo: aprob_modelo
    };
    axios
      .post("/precintos/reg_surtidor", data)
      .then(res => {
        console.log(res);
        console.log("hemos vuelto");
        $("#cif_client").val("");
        $("#marca").val("");
        $("#num_serie").val("");
        $("#modelo").val("");
        $("#año_fabr").val("");
        $("#aprob_modelo").val("");
        
        queryMaquinas(clie_seleccionado);
        $("#modal_add_Surtidor").modal("hide");
        

      })
      .catch(err => {
        console.log(err);
      });
  });
  /* CUANDO SE OCULTA EL MODAL DE AÑADIR SURTIDOR */
//   $("#modal_add_Surtidor").on("hidden.bs.modal", function(e) {
//     console.log("se ha ocultado modal add precintos");
//     valIniSelect("select_surtidor"); /* Inicializo la lista de select_surtidor */
//     // querySurtidores(
//     //   selected
//     // ); /* Realizo la consulta de los surtidores  que tiene el
//     //    * cliente y actualiza la lista del campo select_surtidor */
//   });
  /* *****fin******** */


  /* ACTUALIZAR MAQUINA */
$('#btn_actualizar_maquina').on("click", function (params) {
    event.preventDefault();
    // var _id = id_maquina_selec;
    var cif = $("#edit_cif_client").val();
    var categoria = $("#edit_categoria").val();
    var marca = $("#edit_marca").val();
    var num_serie = $("#edit_num_serie").val();
    var modelo = $("#edit_modelo").val();
    var año_fabicacion = $("#edit_año_fabr").val();
    var aprob_modelo = $("#edit_aprob_modelo").val();
    
    data_update_maquina = {
        cif:                cif,
        categoria:          categoria,
        marca:              marca,
        num_serie:          num_serie,
        modelo:             modelo,
        ano_fabricacion:    año_fabicacion,
        cod_aprob_modelo:   aprob_modelo
    };
    console.log(data_update_maquina);
    axios
        .put(`/clientes/update_maquina/${id_maquina_selec}`, data_update_maquina)
        .then(function (req, res) {
            console.log(res);
            //alert('Se ha actualizado la maquina');
            queryMaquinas(clie_seleccionado); /* Vuelvo a consultar las maquinasx del cliente
                                                * y actualizo la lista  */
            $("#modal_edit_maquina").modal("hide");                                               
        })
        .catch(err => {
            console.log(err, mensaje);
        })
});
/* Desabilitar inputs de formulario editar_cliente
 * para no poder modificarlo, hasta hacer click en 
 * el boton modificar*/

 function desabilFormCliente() {
    
    $('#nume_client').prop("disabled", false);   
    $('#cif').prop("disabled", false);   
    $('#inp_client').prop("disabled", true);   
    $('#direccion').prop("disabled", true);   
    $('#cod_postal').prop("disabled", true);   
    $('#localidad').prop("disabled", true);   
    $('#provincia').prop("disabled", true);   
    $('#email').prop("disabled", true);   
    $('#telefono1').prop("disabled", true); 
};


/* *********************** */