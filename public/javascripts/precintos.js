var arrayprecintos = [];
var prec = $("#num_precinto").val();
var selected; // Variable global para guardar el cif del cliente seleccionado form Añadir Precintos
var categoria = "";
/* Al Cambiar el valor de select cliente PAGE AÑADIR PRECINTOS */
$("#select_client").change(function() {
  selected = $("#select_client option:selected").val();
  console.log(selected);
  // limpiarSelect(select_client);
  //var det = (document.getElementById("select_surtidor").length = 0);
  //console.log(det.length);
  // $("#select_surtidor").empty(); //Elimino los datos del select_Surtidor anteriores
  valIniSelect("select_surtidor");
  querySurtidores(selected); /* Realizo la consulta de los          *surtidores que tiene el cliente y actualizo la lista del campo     *select_surtidor */
  $("#date").removeAttr("disabled"); //habilitar Form Datos AnexoIII
  $("#num_anexo-3").removeAttr("disabled"); //habilitar Form Datos AnexoIII

  /* Evento al seleccionar un elemento del select categoria del modal añadir maquinas */
    $('#select_categoria').on('select2:select', function (e) {
    categoria = e.params.data.text;
    console.log('select_categoria' + e.params.data.text);
  });

});

//}); /* fin document ready() */

/** AL PERDER EL FOCO DEL INPUT Nº DE ANEXOIII */
$("#num_anexo-3").focusout(function() {
  if ($("#num_anexo-3").val() > 0) {
    var data = {
      etiqueta: "anexo",
      mensaje: "El Nº de AnexoIII. Existe en la  Base de Datos"
    };

    var anexoActual = { num_anexo: $("#num_anexo-3").val() };
    axios
      .post("/precintos/existe_anexo3", anexoActual)
      .then(res => {
        console.log("datos -->", res.data.existe);
        if (res.data.existe == 1) {
          //$("#num_anexo-3").val("");
          $("#select_surtidor").attr("disabled", "disabled");
          $("#btn_add_surtidor").attr("disabled", "disabled");
          window.scrollTo(0, 0);
          mensajeError(data);
        } else {
          $("#select_surtidor").removeAttr(
            "disabled"
          );
        /* Habilitar formulario de identificación de Surtidor*/
          $("#btn_add_surtidor").removeAttr("disabled");
          // $("#select_surtidor").focus();
        }
      })
      .catch(err => {
        console.log(err);
      });
  } else {
    $("#select_surtidor").attr("disabled", "disabled");
    $("#btn_add_surtidor").attr("disabled", "disabled");
    $("#num_precinto").attr("disabled", "disabled");
    $("#localizacion").attr("disabled", "disabled");
    $("#add_precintos").attr("disabled", "disabled");

    document.getElementById("num_anexo-3").focus();
  }
});

/**Al cambiar el valor de Selec_surtidor */
$("#select_surtidor").change(function() {
  $("#num_precinto").removeAttr("disabled");
  $("#localizacion").removeAttr("disabled");
  $("#add_precintos").removeAttr("disabled");
});
// ----------------------------



/* ABRIR FORMULARIO MODAL para AÑDIR SURTIDOR */
$("#btn_add_surtidor").on("click", function() {
  $("#modal_add_Surtidor").modal("show");
});

$("#modal_add_Surtidor").on("shown.bs.modal", function() {

  $('#select_categoria').select2 ({
    theme : "bootstrap",
    placeholder: 'Selecciona una categoria',
    data: opcion_cat_surtidores
  });

  var focus_categoria = document.getElementById("select_categoria");
 
  $("#cif_client").val(selected);
  focus_categoria.focus();
});
/**GUARGAR DATOS DE NUEVO SURTIDOR EN BASE DE DATOS*/
$("#btn_guardar_surtidor").on("click", function() {
  event.preventDefault();

  var cif = $("#cif_client").val();
  var marca = $("#marca").val();
  var num_serie = $("#num_serie").val();
  var modelo = $("#modelo").val();
  var año_fabric = $("#año_fabr").val();
  var aprob_modelo = $("#aprob_modelo").val();
  data = {
    cif: cif,
    categoria: categoria, // categoria es una variable global
    marca: marca,
    num_serie: num_serie,
    modelo: modelo,
    ano_fabricacion: año_fabric,
    cod_aprob_modelo: aprob_modelo
  };
  axios
    .post("/precintos/reg_surtidor", data)
    .then(res => {
      console.log(res);
      console.log("hemos vuelto");
      $("#cif_client").val("");
      $("#marca").val("");
      $("#num_serie").val("");
      $("#modelo").val("");
      $("#año_fabr").val("");
      $("#aprob_modelo").val("");
      $("#modal_add_Surtidor").modal("hide");
    })
    .catch(err => {
      console.log(err);
    });
});
/* CUANDO SE OCULTA EL MODAL DE AÑADIR SURTIDOR */
$("#modal_add_Surtidor").on("hidden.bs.modal", function(e) {
  console.log("se ha ocultado modal add precintos");
  valIniSelect("select_surtidor"); /* Inicializo la lista de select_surtidor */
  querySurtidores(
    selected
  ); /* Realizo la consulta de los surtidores  que tiene el
      * cliente y actualiza la lista del campo select_surtidor */
});
/* *****fin******** */



/* AÑADIR PRECINTOS*/
$("#add_precintos").on("click", function() {
  event.preventDefault();
  //console.log($("#num_precinto").val());
  /**Consulta para verificar que no existe nº precinto en Base de datos */
  var data = {
    etiqueta: "precinto",
    mensaje: "El Nº de Precinto. Existe en la  Base de Datos"
  };
  var data2 = {
    etiqueta: "precinto",
    mensaje: "El Nº de Precinto esta ripitio ¬¬' "
  };

  var precintoActual = { num_precinto: $("#num_precinto").val() };
  
  let yaexiste=0;

  for (i=0;i<arrayprecintos.length;i++){
    if (arrayprecintos[i].num_precinto==$("#num_precinto").val()) yaexiste=1;
  }

  if (yaexiste){
    mensajeError(data2);
  }
  else {
    axios
      .post("/precintos/existe_precinto", precintoActual)
      .then(res => {
        console.log("datos -->", res.data.existe);
        if (res.data.existe == 1) {
          mensajeError(data);
        } else {
          arrayprecintos.push({
            num_precinto: $("#num_precinto").val(),
            localizacion: $("#localizacion").val()
          });
          
          $("#precintos_añadidos").append('<li class = "list-grooup-item border ml-1">'+ $('#num_precinto').val() + "    |   " +$('#localizacion').val() + '</li>');
          // document.getElementById('precintos_añadidos').innerHTML += numPrecinto; */

          num_precinto: $("#num_precinto").val("");
          localizacion: $("#localizacion").val("");
          $("#num_precinto").focus();
        }
      })
      .catch(err => {
        console.log(err);
      });
  }
});

/*****Evento al cerrar modal_error ****/


$("#btn_elim_ultimo").on("click", function() {

  //console.log(arrayprecintos.length);
  if (arrayprecintos.length>0){
    let fin = arrayprecintos.length-1;
    $("#precintos_añadidos li").last().remove();
    arrayprecintos.pop();
    console.log(arrayprecintos);
  }

});

/* FUNCIONES */

/* Funcion Inicializar el campo de selección de surtidores */
function valIniSelect(idSelect) {
  document.getElementById(
    idSelect
  ).length = 0; /* Limpio la lista de select_surtidor  */
  var select = document.getElementById(idSelect);
  var option = document.createElement("option");
  option.text = "Introduce Nº de surtidor";
  select.add(option, select[0]);
  console.log("select");
}
/* ---------fin function valIniSelect()--------------- */

/* Consulta de Surtidores de un cliente */
function querySurtidores(cif_cliente) {
  var client = cif_cliente;
  axios.get(`/precintos/get_maquina/${client}`).then(function(res) {
    console.log(res.data.maquina.length);
    let options = "";
    for (var i = 0; i < res.data.maquina.length; i++) {
      options +=
        '<option value="' +
        res.data.maquina[i].num_serie +
        '">' +
        res.data.maquina[i].num_serie +
        "</option>";
    }
    $("#select_surtidor").append(options); //visualiza la lista de Surtidores del Cliente
  });
}
/* -------------fin function querySurtidores()--------------- */
