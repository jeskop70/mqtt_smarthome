const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const User = require('../models/dbUser'); //Instancia al modelo de datos de Usuario
 

passport.use(new LocalStrategy({
    usernameField: 'usuario',
    passwordField: 'password',
    passReqToCallback : true 
}, async (req, usuario, password, done) => {
    const user = await User.findOne({usuario: usuario});
    console.log({user});
    if(!user){
        return done(null, false, req.flash('message', 'Usuario no encontrado'));
    }
    else {
        const match = await user.matchPassword(password);
         if(match) {
            return done(null, user);
        } else {
            return done(null, false, req.flash('message', 'Contraseña no Valida'));
        }
    } 
}));

passport.serializeUser((user, done) => {
    done(null, user.id);
    
});

passport.deserializeUser((id, done) => {
    User.findById(id, (err, user) => {
        done(err, user);
    });
});
     