const createError = require("http-errors");
const express = require("express");
const path = require("path");
//const cookieParser = require("cookie-parser");
const logger = require("morgan");
const bodyParser = require("body-parser");
const exphbs = require("express-handlebars");
const methodOverride = require("method-override");
const session = require("express-session");
const flash = require("connect-flash");
const passport = require("passport");
const cors = require("cors");
const moment = require('moment');
// const helpers = require("./hbs_helpers");
// const axios = require("axios")

//ROUTES
const indexRouter = require("./routes/index.routes");  
const usersRouter = require("./routes/users.routes");
const satRouter = require("./routes/SatRutas.routes");
const precintosRouter = require("./routes/SatPrecintos.routes");
const clientRouter = require('./routes/SatClientes.routes');
const averiasRouter = require('./routes/SatAverias.routes');




//Inicializar express
const app = express();
require("./database"); // llamo al archivo de configuración de la base de datos
require("./config/passport");

/* Inicializar Handlerbars */
const hbs = exphbs.create({
  defaultLayout: "main",
  layoutsDir: path.join(app.get("views"), "layouts"),
  partialsDir: path.join(app.get("views"), "partials"),
  extname: "hbs",
  helpers:{ 
    bar: function(){
      return "BAR";
    },
    if_eq: function(a, b, opts) {
      if (a == b) {
          return opts.fn(this); 
      } else {
          return opts.inverse(this);
      }
    },
    pasafecha: function(fecha){
      //console.log(moment(fecha,'DD/MM/YYYY'));
      return moment(fecha,'DD/MM/YYYY').format('YYYY-MM-DD');
    }
}
  
})


/*  view engine setup */
app.set("views", path.join(__dirname, "views"));
//app.set('view engine', 'ejs');
//Motor de plantillas EXPRESS-HADLERBAR


app.engine(
  ".hbs", hbs.engine
 /*  exphbs({
    defaultLayout: "main",
    layoutsDir: path.join(app.get("views"), "layouts"),
    partialsDir: path.join(app.get("views"), "partials"),
    extname: "hbs"
  }) */
);
app.set("view engine", ".hbs");

/*  MIDDLEWEARS */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false  }));
app.use(methodOverride("_method"));
app.use(session({
    secret: "mysecretapp",
    resave: true,
    saveUninitialized: true
  }));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());  
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());
//app.use(cookieParser());

/* Variables Globales */
app.use((req, res, next) => {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.message = req.flash('message');
  res.locals.user = req.user || null;
  next();
});

/* Statics Files */
app.use(express.static(path.join(__dirname, "public")));
app.use('/css', express.static(__dirname + '/node_modules/select2/dist/css/'));
app.use('/css', express.static(__dirname + '/node_modules/select2-theme-bootstrap4/dist/'));
// app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css/'));
// app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js/'));
// app.use('/js', express.static(__dirname + '/node_modules/jquery/dist/'));
app.use('/js', express.static(__dirname + '/node_modules/@popperjs/core/dist/umd/'))

/* ROUTES */
app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/rutas", satRouter);
app.use("/precintos", precintosRouter);
app.use("/clientes", clientRouter);

// catch 404 and forward to error handler
/* app.use(function(req, res, next) {
  next(createError(404));
}); */

/*  error handler */
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  /* res.status(err.status || 500);
  res.render("error"); */
});

module.exports = app;
